﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser : Bullet {


	public float lifetime=5.0f;

	public float rangeOfLaser;

	public Vector2 startPosition{ get; set; }

	public GameObject parent{ get; set;}

	private LineRenderer lineRender;
	private int layerMask = ~9;


	protected void Awake ()
	{
		Destroy (gameObject, lifetime);
		lineRender = GetComponent<LineRenderer> ();
	}

	protected void Update()
	{	
		if (parent != null) {
			lineRender.SetPosition (0,startPosition);
			lineRender.SetPosition (1, new Vector2 (startPosition.x, startPosition.y - rangeOfLaser));
		

			RaycastHit2D bam = Physics2D.Raycast 
				(startPosition, new Vector2 (startPosition.x, (startPosition.y - rangeOfLaser)),
					rangeOfLaser, layerMask);
			if (bam && bam.collider.tag!=parentTag) {	
				lineRender.SetPosition (0, startPosition);
				lineRender.SetPosition (1, bam.point);
				bam.collider.GetComponent<Unit> ().GetDamage (damage);
			}
		}
	}


}
