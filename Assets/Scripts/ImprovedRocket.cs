﻿using UnityEngine;

public class ImprovedRocket : Rocket {
	
	public float explosionRadiusMultiplier;

	protected override void OnTriggerEnter2D (Collider2D collider)
	{
		Collider2D myColl = gameObject.GetComponent<Collider2D>();
		myColl.bounds.extents.Scale (new Vector3 (explosionRadiusMultiplier, explosionRadiusMultiplier));

		base.OnTriggerEnter2D (collider);
	}


}
