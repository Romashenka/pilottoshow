﻿using UnityEngine;

public class DestroyOverTime : MonoBehaviour {

	public float lifetime;

	void Awake () {
		Destroy (gameObject, lifetime);
	}

}
