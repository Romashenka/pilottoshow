﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[DisallowMultipleComponent]
public class BackGroundScript : MonoBehaviour {

	[Range(-1,1)]
	public float speed = 0.5f;
	Vector2 offset=Vector2.zero;
	Material mat;



	// Use this for initialization
	void Start () {
		mat = GetComponent<MeshRenderer>().material;

	}

	// Update is called once per frame
	void FixedUpdate () {
		ScrollMaterialByTime (mat);
	}

	public void ScrollMaterialByTime(Material mat){
		
		offset=new Vector2(0,-Time.time*speed);
	
		mat.mainTextureOffset = offset;
	}
}
