﻿using System;
using UnityEngine;



public class Glooobal : MonoBehaviour {

	public static bool newGameMode = true;
	public static int killingsCount{ get; private set;}
	public static string leavedScene{ get; private set;}
	public static int money { get; set;}

	public Action<EnemyDieEventArgs> OnEnemyDie;


	public static Glooobal instance;

	void Start(){
		if (instance != null) {
			Destroy (gameObject);
			Debug.Log ("More than one gloobal instance");
			return;
		} else {
			instance = this;
			OnEnemyDie += Gloobal_OnEnemyDies;
		}
	}

	void Gloobal_OnEnemyDies (EnemyDieEventArgs e)
	{
		killingsCount++;
		money += e.price;
	}

	public static void ZeroStats(){
		killingsCount = 0;
		money = 0;
	}

	public static void ZeroKillings(){
		killingsCount = 0;
	}

	public static class GloobalSaver
	{
		public static void SaveProgress(Character character){
			PlayerPrefs.DeleteAll ();
			PlayerPrefs.SetInt ("Rockets", character.rockets);
			PlayerPrefs.SetInt ("ImprovedRockets", character.improvedRockets);
			PlayerPrefs.SetFloat ("Armor", character.armor);
			PlayerPrefs.SetInt ("Money", money);
			PlayerPrefs.SetInt ("LeavedScene", UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex+1);
			PlayerPrefs.Save ();
		}


		public static void LoadProgress(Character character){
			character.SetStats (
				PlayerPrefs.GetInt("Rockets"),
				PlayerPrefs.GetInt("ImprovedRockets"),
				PlayerPrefs.GetFloat("Armor"));
			money = PlayerPrefs.GetInt ("Money");
			ZeroKillings ();
		}
	}

	public class EnemyDieEventArgs : EventArgs{

		public int price;

		public EnemyDieEventArgs(int price){
			this.price = price;
		}
	}

}
