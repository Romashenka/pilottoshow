﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class LaserTurett : LaserEnemy {
	
	public Slider healthBar;

	public override void Die ()
	{
		Boss boss = GetComponentInParent<Boss> ();
		boss.TurretDown();
		base.Die ();
	}

	new protected void Awake(){
		laser = Resources.Load<Laser> ("Laser");
		healthBar.maxValue = health;
	}

	protected override void OnTriggerEnter2D (Collider2D collider)
	{
		base.OnTriggerEnter2D (collider);
		healthBar.value = health;
	}
}
