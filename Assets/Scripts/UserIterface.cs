﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UserIterface : MainMenu {

	public GameObject player;

	public int killingBorder;

	[Header("Menus")]
	public GameObject pauseMenu;

	public GameObject storeMenu;

	[Header("Shop Buttons")]
	public Button buyRocketsButt;

	public Button buyImprRocketsButt;

	public Button buyArmorButt;

	[Header("Sliders")]
	public Slider healthBar;

	public Slider armorBar;

	[Header ("Counters")]
	public Text moneyCounter;

	public Text rocketsCounter;

	public Text improvedRocketsCounter;

	public Text killingsCounter;

	[Header("EventPanels")]
	public GameObject loosePanel;

	public GameObject victoryPanel;

	public GameObject nextLevelPanel;

	Character character;
	GameObject boss;
	Scene currentScene;



	void Start () {
		Time.timeScale = 1;

		if (player == null)
			player = GameObject.Find ("CharacterObject");
		pauseMenu.SetActive (false);
		storeMenu.SetActive (false);
		character = player.GetComponent<Character> ();
		character.enabled = true;
		buyRocketsButt.interactable = false;
		buyImprRocketsButt.interactable = false;
		buyArmorButt.interactable = false;
		currentScene = SceneManager.GetActiveScene ();
		victoryPanel.SetActive (false);
		nextLevelPanel.SetActive (false);

		if (currentScene.name == "BossWave")
			boss = GameObject.Find ("BigBoss");
	}


	void Update () {

		UpdateUI ();

		//Scene Management
		if (character == null)
			loosePanel.SetActive (true);
		if (Glooobal.killingsCount >= killingBorder && currentScene.name != "BossWave") {
			Time.timeScale = 0;
			nextLevelPanel.SetActive (true);
			if (Input.GetKeyDown (KeyCode.Space)) {
				Time.timeScale = 1;
				NextScene ();
			}
		} else if (currentScene.name == "BossWave" && boss == null) {
			Time.timeScale = 0;
			victoryPanel.SetActive (true);
		}

	}

	void NextScene(){
		SceneManager.LoadScene (currentScene.buildIndex + 1, LoadSceneMode.Single);
		Glooobal.GloobalSaver.SaveProgress (character);
		Glooobal.ZeroKillings();

	}

	void UpdateUI(){
		if (!pauseMenu.activeSelf && !storeMenu.activeSelf) {
			if (Input.GetButtonDown ("Cancel")) {
				Time.timeScale = 0;
				pauseMenu.SetActive (true);
				character.enabled = false;
			}
		}
		if (Glooobal.money > 50)
			buyRocketsButt.interactable = true;
		if (Glooobal.money > 100)
			buyImprRocketsButt.interactable = true;
		if (Glooobal.money > 150)
			buyArmorButt.interactable = true;
		moneyCounter.text = Glooobal.money.ToString();
		rocketsCounter.text = character.rockets.ToString();
		improvedRocketsCounter.text = character.improvedRockets.ToString();
		healthBar.value = character.health;
		armorBar.value = character.armor;
		killingsCounter.text = Glooobal.killingsCount.ToString();
	}


	#region ButtonsMethods
	public override void Continue(){
		character.enabled = true;
		pauseMenu.SetActive (false);
		Time.timeScale = 1;
	}

	public void RestartLevel(){
		SceneManager.LoadScene (currentScene.buildIndex,LoadSceneMode.Single);
	}

	public override void Quit(){
		SceneManager.LoadScene (0,LoadSceneMode.Single);
	}

	public void OpenStore(){
		pauseMenu.SetActive (false);
		storeMenu.SetActive (true);
	}

	public void CloseStore(){
		pauseMenu.SetActive (true);
		storeMenu.SetActive (false);
	}

	public void BuyRockets(int price){
		if (Glooobal.money >= price)
		{
			character.AddItems (1, 0, 0);
			Glooobal.money -= price;
		}
	}

	public void BuyImprRockets(int price){
		if (Glooobal.money >= price)
		{
			character.AddItems (0, 1, 0);
			Glooobal.money -= price;
		}
	}

	public void BuyArmor(int price){
		if(Glooobal.money >= price && character.armor < 100.0f)
		{
			character.AddItems(0,0,20);
			Glooobal.money -= price;
		}
	}
	#endregion
}
