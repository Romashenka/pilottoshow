﻿using System;
using UnityEngine;


public class Enemy : Unit {


	[SerializeField]
	protected float speed=1.0f;

	[SerializeField]
	protected float shootFreq;
	protected float time=0;

	[SerializeField]
	protected float bumpIntoDmg=3.0f; //dmg to object what bunm into gameobject

	[SerializeField]
	protected int price;

	protected Bullet bullet;


	protected virtual void Awake () 
	{
		bullet = Resources.Load <Bullet>("ShotBullet");
	}
	

	protected void Update () 
	{
		StayOnScreen ();

		Move ();

		if (Time.time > time + shootFreq) {
			time = Time.time;
			foreach (GameObject gT in gunTips) {
				Shoot (gT.transform);
			}
		}
	}

	protected void Move()
	{
		Vector3 direction = transform.up * (-1.0f);
		transform.position = Vector3.MoveTowards (transform.position, transform.position + direction, speed * Time.deltaTime);
	}


	protected virtual void OnTriggerEnter2D(Collider2D collider)
	{
		Unit unit = collider.GetComponent<Unit> ();

		if (unit && unit is Character) 
		{
			unit.GetDamage (bumpIntoDmg);
			GetDamage(bumpIntoDmg/3);
		}
	}

	protected virtual void Shoot(Transform whatFrom){
		Bullet newBullet = Instantiate (bullet, whatFrom.transform.position, whatFrom.transform.rotation) as Bullet;
		newBullet.parentTag = tag;
		newBullet.direction = newBullet.transform.up;
	}

	public override void Die ()
	{
		Glooobal.instance.OnEnemyDie(new Glooobal.EnemyDieEventArgs(price));

		base.Die ();
	}

}
