﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MotherOfEnemies : MonoBehaviour {
	
	public float borningPeriod;

	public float delay;

	public List <GameObject> enemies;


	float time=0;

	Vector3 position;
	float minXBoundary;
	float maxXBoundary;

	void Start () {
		minXBoundary = Camera.main.ScreenToWorldPoint (new Vector2 (0, 0)).x;
		maxXBoundary = Camera.main.ScreenToWorldPoint (new Vector2 (Screen.width, 0)).x;
		time = Time.time+delay;
	}

	void Update () {
		BornEnemies ();
	}

	void BornEnemies(){
		if (Time.time > time + borningPeriod) {
			time = Time.time;
			position=new Vector2(Random.Range(minXBoundary,maxXBoundary),transform.position.y);
			if (enemies.Count > 0)
				Instantiate (enemies [Random.Range (0, enemies.Count)], position, transform.rotation);
		}
	}

}
