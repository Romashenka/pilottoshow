﻿using System.Collections.Generic;
using UnityEngine;

public class Unit : MonoBehaviour {

	public GameObject dyingExplosion;

	public List <GameObject> gunTips;

	[SerializeField]
	protected float myHealth;

	public float health{get{ return myHealth;}}

	public virtual void GetDamage(float damage)
	{
		myHealth -= damage;
		if (myHealth <= 0)
			Die ();
	}

	protected virtual void StayOnScreen(){
		if (transform.position.y < Camera.main.ScreenToWorldPoint (new Vector2 (0, 0)).y)
			Destroy(gameObject);
	}

	public virtual void Die()
	{
		Destroy(gameObject);
		Instantiate (dyingExplosion,transform.position,transform.rotation);
	}
}
