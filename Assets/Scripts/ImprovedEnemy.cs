﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImprovedEnemy : Enemy {
	
	public float burstFreq;


	protected override void Shoot (Transform whatFrom)
	{
		StartCoroutine(BurstCoroutine(burstFreq,whatFrom));
	}

	IEnumerator BurstCoroutine(float sec,Transform shootTransform) {
		
		base.Shoot (shootTransform);
		yield return new WaitForSeconds (sec);
		base.Shoot (shootTransform);
		yield return new WaitForSeconds (sec);

	}
}
