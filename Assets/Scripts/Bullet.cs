﻿using UnityEngine;

public class Bullet : MonoBehaviour
{
	
	public string parentTag {get;set;}
	protected Vector3 myDirection;
	public Vector3 direction { set { myDirection = value; } }



	[SerializeField]
	protected float speed=5.0f;

	[SerializeField]
	protected float myDamage=1.0f;
	public float damage{ get{return myDamage;}}


	virtual protected void FixedUpdate()
	{
		StayOnScreen ();
		transform.position = Vector3.MoveTowards (transform.position, transform.position + myDirection, speed * Time.deltaTime);

	}

	protected virtual void StayOnScreen(){
		if (transform.position.y < Camera.main.ViewportToWorldPoint (new Vector2 (0, 0)).y)
			Destroy(gameObject);
		if(transform.position.y>Camera.main.ViewportToWorldPoint(new Vector2(0,1)).y)
			Destroy(gameObject);
	}

	virtual protected void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject != null) {
			Unit unit = other.gameObject.GetComponent<Unit> ();
			if (other.tag != parentTag&&other.tag!=tag) {
				unit.GetDamage(damage);
				Destroy (gameObject);
			}
		}
	}


}





