﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserEnemy : Enemy {

	protected Laser laser;


	new protected void Awake ()
	{
		laser = Resources.Load<Laser> ("Laser");
	}

	protected override void Shoot (Transform whatFrom)
	{
		Laser newLaser = Instantiate (laser,whatFrom.position,whatFrom.rotation,transform) as Laser;
		newLaser.startPosition = whatFrom.position;
		newLaser.parent = gameObject;
		newLaser.parentTag = tag;
	}

	// OnTriggerEnter don't work without it.
	protected override void OnTriggerEnter2D (Collider2D collider)
	{
		base.OnTriggerEnter2D (collider);
	}

}
