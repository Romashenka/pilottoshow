﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Boss : Unit {

	public Slider HealthBar;

	public float bumpIntoDmg;

	int turretCount;

	void Start(){
		HealthBar.maxValue = health;
		HealthBar.gameObject.SetActive(false);
		turretCount = GetComponentsInChildren<Enemy> ().Length;
	}

	public void TurretDown(){
		turretCount--;
	}

	public override void GetDamage (float damage)
	{
		if (turretCount==0) {
			base.GetDamage (damage);
			HealthBar.gameObject.SetActive (true);
			HealthBar.value = health;
		}
		else
			return;
	}

	protected void OnTriggerEnter2D(Collider2D collider)
	{
		Unit unit = collider.GetComponent<Unit> ();

		if (unit && unit is Character) 
		{
			unit.GetDamage (bumpIntoDmg);
			GetDamage(bumpIntoDmg/3);
		}

	}
}
