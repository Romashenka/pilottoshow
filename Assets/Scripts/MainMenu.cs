﻿using UnityEngine.SceneManagement;
using UnityEngine;

public class MainMenu : MonoBehaviour {
	
	public void NewGame(){
		Glooobal.newGameMode = true;
		SceneManager.LoadScene (1,LoadSceneMode.Single);
		Glooobal.ZeroStats ();
		Time.timeScale = 1;
	}

	public virtual void Continue(){

		Glooobal.newGameMode = false;
		SceneManager.LoadScene(PlayerPrefs.GetInt("LeavedScene"),LoadSceneMode.Single);
		Time.timeScale = 1;
	}

	public virtual void Quit(){
		Application.Quit();
	}

}
