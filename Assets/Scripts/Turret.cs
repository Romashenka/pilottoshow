﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class Turret : Enemy {
	
	public Slider healthBar;

	public override void Die ()
	{
		Boss boss = GetComponentInParent<Boss> ();
		boss.TurretDown();

		base.Die ();
	}

	new void Awake(){
		bullet = Resources.Load <Bullet>("ShotBullet");
		healthBar.maxValue = health;
	}

	protected override void OnTriggerEnter2D (Collider2D collider)
	{
		base.OnTriggerEnter2D (collider);
		healthBar.value = health;
	}

}
