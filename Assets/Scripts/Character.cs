﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void CharDies();

public class Character : Unit 
{
	[SerializeField]
	private float myArmor;
	public float armor{get{return myArmor; }}

	[SerializeField]
	private int myRockets;
	public int rockets{get{return myRockets; }}

	[SerializeField]
	private int myImprovedRockets;
	public int improvedRockets{get{return myImprovedRockets; }}

	[SerializeField]
	private float speed;

	[SerializeField]
	private float shootFreq;

	[SerializeField]
	private float rocketShootFreq;
	public GameObject shieldSprite;


	private GameObject bulletPrefab;
	private GameObject rocketPrefab;
	private GameObject imprRocketPrefab;
	private CircleCollider2D shieldCollider;

	private float time=0.0f;//for shooting




	void Awake () 
	{
		if (!Glooobal.newGameMode) {
			Glooobal.GloobalSaver.LoadProgress (this);
			myHealth = 150;

		} 
		else {
			myHealth = 150;
			Glooobal.ZeroStats();
			Glooobal.GloobalSaver.SaveProgress (this);
			Glooobal.newGameMode = false;
		}

		shieldCollider = GetComponent<CircleCollider2D>();
		bulletPrefab = Resources.Load <GameObject>("ShotBullet");
		rocketPrefab= Resources.Load<GameObject> ("Rocket");
		imprRocketPrefab = Resources.Load<GameObject> ("ImprovedRocket");
	}
	
	void FixedUpdate () 
	{
		StayOnScreen ();	
		
		if (Input.GetButton ("Horizontal"))
			MoveHorizontal ();
		if (Input.GetButton ("Vertical"))
			MoveVertical ();
		if (Input.GetButton ("Fire1"))
		{
			if (Time.time > time + shootFreq) 
			{
				time = Time.time;
				foreach (GameObject gunTip in gunTips) {
					Shoot (gunTip.transform,bulletPrefab);
				}
			}
		}

		if(Input.GetButton("Fire2")){
			if (Time.time > time + rocketShootFreq) {
				time = Time.time;
				if (myImprovedRockets > 0) {
					Shoot (transform, imprRocketPrefab);
					myImprovedRockets--;
				} else if (myRockets > 0) {
					Shoot (transform, rocketPrefab);
					myRockets--;
				}
			}
		}


		//for shields
		if (armor > 0) {
			shieldCollider.enabled = true;
			shieldSprite.SetActive (true);
		} else {
			shieldCollider.enabled = false;
			shieldSprite.SetActive (false);
		}
	}


	new protected void StayOnScreen ()
	{
		float minXpoint = Camera.main.ScreenToWorldPoint (new Vector2 (0, 0)).x;

		float maxXpoint = Camera.main.ScreenToWorldPoint (new Vector2 (Screen.width, 0)).x;

		float minYpoint = Camera.main.ScreenToWorldPoint (new Vector2 (0, 20)).y;

		float maxYpoint = Camera.main.ScreenToWorldPoint (new Vector2 (0, Screen.height)).y;

		transform.position = new Vector2 (Mathf.Clamp (transform.position.x, minXpoint, maxXpoint),transform.position.y);
		transform.position = new Vector2 (transform.position.x,Mathf.Clamp (transform.position.y, minYpoint, maxYpoint));

	}

	public void AddItems(int rockets,int improvedRockets, float armor){
		myRockets += rockets;
		myImprovedRockets += improvedRockets;
		myArmor += armor;
		myArmor = Mathf.Clamp (myArmor, 0.0f, 100.0f);
	}


	public void SetStats(int rockets, int improvedRockets, float armor){
		this.myRockets = rockets;
		this.myImprovedRockets = improvedRockets;
		this.myArmor = armor;
	}

	private void MoveHorizontal ()
	{
		Vector3 direction = transform.right * Input.GetAxis ("Horizontal");
		transform.position = Vector3.MoveTowards (transform.position, transform.position + direction, speed * Time.deltaTime);
	}

	private void MoveVertical ()
	{
		Vector3 direction = transform.up * Input.GetAxis ("Vertical");
		transform.position = Vector3.MoveTowards (transform.position, transform.position + direction, speed * Time.deltaTime);
	}



	private void Shoot(Transform whatFrom, GameObject projectile)
	{
		Bullet newProjectile = Instantiate (projectile, whatFrom.position, whatFrom.rotation).GetComponent<Bullet> ();
		newProjectile.parentTag = tag;
		newProjectile.direction = projectile.transform.up;
	}

	public override void GetDamage(float damage)
	{
		if (myArmor > 0) {
			myArmor -= damage;
		} else
			base.GetDamage (damage);
	}

	public override void Die ()
	{
		
		base.Die ();
		
	}


}

